---
title: "Chicken Ramen"
date: 2019-12-26T18:17:42-06:00
authors:
  - Keith Wickramasekara
draft: false
hero: images/hero.jpg
timeToRead: 5
excerpt: This is one of my favors recipes because I love ramen eggs and the taste it makes when the egg yolk mixes wth the broth. Preparing the egg may take a while otherwise its a quick and easy recipe.
---

This is one of my favorite recipes because I love ramen eggs and the taste it makes when the egg yolk mixes wth the broth. Preparing the eggs may take a while otherwise its a quick and easy recipe.

## Ingredients (2 Servings)

- Chicken breast (2 servings)
- Eggs (2)
- Chicken stock (3 cups)
- Red onions (1/2)
- Carrots (2)
- Peas (3/4 cups)
- Mushroom (1 cup)
- Ginger (1tsp)
- Garlic (3 cloves)
- Soy Sauce (6tbsp)
- Oyster sauce (4tbsp)
- Olive oil (4tbsp)
- Thyme (1 tsp)
- Seaweed (6 pieces)
- Red pepper flakes (1tbsp)
- Salt and pepper
- Seasame seed or green onions

## Ramen Eggs(Ajitsuke Tamago)

1. Bring water to a boil in a pan. You want the water to cover the whole egg.
2. Let them cook for exactly 6 mins. Turn over the eggs around 3mins.
3. Get the eggs out and put themm in an ice bath for 3mins. This stops the eggs from cooking further. To make the ice bath put ice and water to a level where it covers the whole egg.
4. Take the eggs out and peel.
5. Get another bowl and create the egg marinage by mixing soy sauce(4 tbsp), oyster sauce(4 tbsp) and water(6 tbsp)
6. Submerge eggs in the marinade and cover with plastic wrap.
7. Let them sit in the fridge for atleast 6 hours. Overnight would be ideal.

## Chicken Ramen

1. In a large pan mix chicken stock(3 cups), water(1 cup) and soy sauce(2 tbsp)
2. Add lightly chopped garlic(2 cloves) and ginger(1tsp minced)
3. Mix carrots(2 carrots cut into circles), red onions(1/2 onion finely chopped), mushroom(1 cups diced), red pepper flakes(1 tbsp) and ramen(2 packets)
4. Let it cook until ramen is soft.
5. Prepare the diced chicken breast\* in a separate pan. Season it with salt and pepper. Add olive oil(4 tbsp) to the pan with a clove of garlic and thyme. Let it cook until both sides are golden brown.
6. Once ramen is soft, mix the cooked chicken and peas(3/4 cup). Let it cook for another 2mins.
7. Add salt and pepper to taste.
8. Add one serving to a plate with some broth. Cut the eggs in half and place them in the plate along with seaweed(3 pieces).
9. Garnish with seasame seed or green onions.

\*_Do not cook chicken right out of the fridge. Bring them to room temperature by letting it sit for atleast 30 mins._
