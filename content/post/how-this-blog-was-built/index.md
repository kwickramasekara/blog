---
title: "How this blog was built"
date: 2019-11-29T20:35:24-06:00
authors:
  - Keith Wickramasekara
draft: false
hero: images/hero.jpg
timeToRead: 2
excerpt: A quick look into the recent updates and the tools I use to publish this blog.
---

This blog used to be on a self-hosted version of [ghost](https://ghost.org) which is a CMS powered by node and SQL. It worked fine and did exactly what you'd need from a no-frills publishing platform. But, I do have a few other things running on this server and I kept adding to it which increased the server load. Around the same time, I got to know about static site generators and wanted to test things out. As I dug through the rabbit hole, I found [Hugo](https://gohugo.io) which promised less than a second build times. Then I looked into their theme gallery cause I didn't want to spend time designing one. Fell in love with [Novela](https://themes.gohugo.io/hugo-theme-novela/) and as they say, the rest was history.

![Build time](images/build-time.png)

As you can see above, a complete build only took 109 milliseconds to process 17 pages and 47 images. Hot-reloads are even quicker because it only processes changed files and they are usually below 10 milliseconds. So overall I'm very happy with its performance.

Publishing experience is also better because I write markdown and Hugo does the hot-reload magic and I'm able to instantly see changes in the browser. Once I'm done writing, all I have to do is run the NPM `deploy` command which I have set up to do a build and a SSH copy to the remote server. I might look into handling this devops situation with a CI/CD pipeline later but I don't have any complaints for now. It all worked out perfect for me and now there's one less thing eating up resources on the server.

Some things I like about [Hugo](https://gohugo.io):

- Crazy fast builds :rocket:
- Built-in Image optimizations :hammer:
- Shortcode and Markdown support :memo:
- Good collection of themes :art:
- [Very easy to learn](https://www.youtube.com/watch?v=qtIqKaDlqXo&list=PLLAZ4kZ9dFpOnyRlyS-liKL5ReHDcj4G3) :book:

P.S: Codebase of this blog is open-sourced @ [Gitlab](https://gitlab.com/kwickramasekara/blog)
